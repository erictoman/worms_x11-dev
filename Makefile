run: example.cpp Serpiente.o Coordenada.o Gfx.o
	g++ example.cpp Serpiente.o Coordenada.o Gfx.o -o example -lX11

Serpiente.o: Serpiente.cpp Coordenada.o
	g++ Serpiente.cpp -c 

Coordenada.o: Coordenada.cpp
	g++ Coordenada.cpp -c 

Gfx.o: Gfx.cpp
	g++ Gfx.cpp -c 