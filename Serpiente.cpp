#include "Serpiente.h"
#include <stdlib.h>

Serpiente::Serpiente(Coordenada c, int t,Gfx &d){
    tam=t;
    last=c;
    display=d;
}
Serpiente::Serpiente(int t,Gfx &d){
    tam=t;
    display=d;
    this->RandomPoint();
    this->movement(rand()%13+1,0);
}

int Serpiente::getTam(){
    return tam;
}

Coordenada Serpiente::getLast(){
    return last;
}

void Serpiente::SetLast(Coordenada c){
    last=c;
}

void Serpiente::RandomPoint(){
    this->SetLast(Coordenada(rand()%display.gfx_xsize(),rand()%display.gfx_ysize()));
}

void Serpiente::movement(int t,int r){
    tipo=t;
    if(r==0){
        radio=rand()%this->getTam()/2;
    }else{
        radio=r;
    }
}

void Serpiente:: mover(){
    if(this->getLast().obtenerX()>display.gfx_xsize() || this->getLast().obtenerY()>display.gfx_ysize() || this->getLast().obtenerX()<=0 || this->getLast().obtenerY()<=0){
        this->RandomPoint();
        this->movement(rand()%13+1,this->radio);
    }else{
        switch (tipo){
            case 1://Mov esquina inferior derecha
                for(int i=0;i<this->getTam();i++){
                    display.gfx_point(this->getLast().obtenerX()+i,this->getLast().obtenerY()+i);
                }
                this->SetLast(Coordenada(this->getLast().obtenerX()+1,this->getLast().obtenerY()+1));
                break;
            case 2://Mov esquina superior derecha
                for(int i=0;i<this->getTam();i++){
                    display.gfx_point(this->getLast().obtenerX()+i,this->getLast().obtenerY()-i);
                }
                this->SetLast(Coordenada(this->getLast().obtenerX()+1,this->getLast().obtenerY()-1));
                break;
            case 3://Mov esquina inferior izquierda
                for(int i=0;i<this->getTam();i++){
                    display.gfx_point(this->getLast().obtenerX()-i,this->getLast().obtenerY()+i);
                }
                this->SetLast(Coordenada(this->getLast().obtenerX()-1,this->getLast().obtenerY()+1));
                break;
            case 4://Mov esquina superior izquierda
                for(int i=0;i<this->getTam();i++){
                    display.gfx_point(this->getLast().obtenerX()-i,this->getLast().obtenerY()-i);
                }
                this->SetLast(Coordenada(this->getLast().obtenerX()-1,this->getLast().obtenerY()-1));
                break;
            case 5://Mov derecha serpenteo
                for(int i=0;i<this->getTam();i++){
                    display.gfx_point(this->getLast().obtenerX()+i,this->getLast().obtenerY()+radio*sin((i+this->getLast().obtenerX())*(3.1415/radio)));
                }
                this->SetLast(Coordenada(this->getLast().obtenerX()+1,this->getLast().obtenerY()));
                break;
            case 6://Mov izquierda serpenteo
                for(int i=0;i<this->getTam();i++){
                    display.gfx_point(this->getLast().obtenerX()+i*-1,this->getLast().obtenerY()+radio*sin((i*-1+this->getLast().obtenerX())*(3.1415/radio)));
                }
                this->SetLast(Coordenada(this->getLast().obtenerX()-1,this->getLast().obtenerY()));
                break;
            case 7://Mov arriba serpenteo
                for(int i=0;i<this->getTam();i++){
                    display.gfx_point(this->getLast().obtenerX()+radio*sin((i*-1+this->getLast().obtenerY())*(3.1415/radio)),this->getLast().obtenerY()+i*-1);
                }
                this->SetLast(Coordenada(this->getLast().obtenerX(),this->getLast().obtenerY()-1));
                break;
            case 8://Mov abajo serpenteo
                for(int i=0;i<this->getTam();i++){
                    display.gfx_point(this->getLast().obtenerX()+radio*sin((i+this->getLast().obtenerY())*(3.1415/radio)),this->getLast().obtenerY()+i);
                }
                this->SetLast(Coordenada(this->getLast().obtenerX(),this->getLast().obtenerY()+1));
                break;
            case 9://Mov derecha
                for(int i=0;i<this->getTam();i++){
                    display.gfx_point(this->getLast().obtenerX()+i,this->getLast().obtenerY());
                }
                this->SetLast(Coordenada(this->getLast().obtenerX()+1,this->getLast().obtenerY()));
                break;
            case 10://Mov izquierda
                for(int i=0;i<this->getTam();i++){
                    display.gfx_point(this->getLast().obtenerX()-i,this->getLast().obtenerY());
                }
                this->SetLast(Coordenada(this->getLast().obtenerX()-1,this->getLast().obtenerY()));
                break;
            case 11://Mov arriba
                for(int i=0;i<this->getTam();i++){
                    display.gfx_point(this->getLast().obtenerX(),this->getLast().obtenerY()-i);
                }
                this->SetLast(Coordenada(this->getLast().obtenerX(),this->getLast().obtenerY()-1));
                break;
            case 12://Mov abajo
                for(int i=0;i<this->getTam();i++){
                    display.gfx_point(this->getLast().obtenerX(),this->getLast().obtenerY()+i);
                }
                this->SetLast(Coordenada(this->getLast().obtenerX(),this->getLast().obtenerY()+1));
                break;
            case 13://Serpenteo
                for(int i=0;i<this->getTam();i++){
                    display.gfx_point(this->getLast().obtenerX()+radio*sin((i+this->getLast().obtenerY())*(3.1415/radio)),this->getLast().obtenerY()+i);
                }
                this->SetLast(Coordenada(this->getLast().obtenerX()+1,this->getLast().obtenerY()+1*2));
                break;
            default:
                break;
        }
    }
}