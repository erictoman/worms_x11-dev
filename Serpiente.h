#ifndef SERPIENTE_H_
#define SERPIENTE_H_
#include "Coordenada.h"
#include <cmath>
#include "Gfx.h"
class Serpiente{
    public:
        Serpiente(Coordenada c,int t,Gfx &d);
        Serpiente(int t,Gfx &d);
        Coordenada getLast();
        void SetLast(Coordenada nueva);
        int getTam();
        void RandomPoint();
        void movement(int tipo,int r);
        void mover();
    private:
        Coordenada last;
        int tam;
        Gfx display;
        int tipo;
        int radio;
};
#endif