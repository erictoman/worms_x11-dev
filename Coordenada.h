#ifndef COORDENADA_H_
#define COORDENADA_H_

class Coordenada{
	private:  
		double x;
		double y;
	public:
	Coordenada();
	Coordenada(double x, double y);
	double obtenerX();
	double obtenerY();
};
#endif