#include <unistd.h>
#include <stdio.h>
#include "Gfx.h"
#include <stdlib.h>
#include <iostream>
#include "Serpiente.h"
#include <vector>
using namespace std;
int main(int argc,char* argv[]/*Height,Width,Lenght,Worms number*/){
    Gfx display = Gfx();
    int h =strtol(argv[1], NULL, 10);//Display height
    int w =strtol(argv[2], NULL, 10);//Display width
    int l =strtol(argv[3], NULL, 10);//Worm lenght
    int n =strtol(argv[4], NULL, 10);//Worm lenght
    display.gfx_open(h,w, "Worms");
    display.gfx_color(0,200,100);
    vector<Serpiente> serpientes;
    for(int i = 0;i<n;i++){
        serpientes.push_back(Serpiente(l,display));
    }
    for(;;){
        display.gfx_clear();
        for(int i = 0;i<serpientes.size();i++){
            Serpiente & s = serpientes.at(i);
            s.mover();
        }
        display.gfx_flush();
        usleep(41666);//24 por segundo
    }
    return 0;
}

