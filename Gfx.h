#ifndef GFX_H_
#define GFX_H_
#include <X11/Xlib.h>
class Gfx{
public:
    Gfx();
    void gfx_open( int width, int height, const char *title );
    void gfx_point( int x, int y );
    void gfx_line( int x1, int y1, int x2, int y2 );
    void gfx_color( int red, int green, int blue );
    void gfx_clear();
    void gfx_clear_color( int red, int green, int blue );
    char gfx_wait();
    int gfx_xpos();
    int gfx_ypos();
    int gfx_xsize();
    int gfx_ysize();
    int gfx_event_waiting();
    void gfx_flush();
private:
        Display *gfx_display;
        Window  gfx_window;
        GC      gfx_gc;
        Colormap gfx_colormap;
        int      gfx_fast_color_mode;
        int saved_xpos;
        int saved_ypos;
        int ysize;
        int xsize;
};
#endif
